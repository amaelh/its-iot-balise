#!/bin/bash
#
#################################################
# Raspberry pi Zerto initialisation script
# This script needs root privileges
# 
# @Author A. Heiligtag
# 
################################################

set -x

# Variables


# Inner functions

######################################################################
# Setup Proxy
######################################################################
function setupProxy() {
vl_user=""
vl_password=""

echo "Please enter proxy user : "
read vl_user

echo "Please enter proxy password : "
read -s vl_password
echo

cat > /etc/apt/apt.conf.d/10proxy << EOF
Acquire::http::Proxy "http://${vl_user}:${vl_password}@your.proxy.domain.tld:8080/";
Acquire::https::Proxy "http://${vl_user}:${vl_password}@your.proxy.domain.tld:8080/";
EOF

cat > /etc/environment << EOF
export {http,https,ftp}_proxy="http://${vl_user}:${vl_password}@your.proxy.domain.tld:8080/"
EOF
}

function unsetupProxy() {
	echo "" > /etc/apt/apt.conf.d/10proxy
	echo "" > /etc/environment
}

######################################################################
# Configure network : Windows connection sharing over USB
######################################################################
function setupNetwork() {
cat > /etc/network/interfaces.d/usb0 << EOF
auto lo
iface lo inet loopback

allow-hotplug usb0
iface usb0 inet static

## Windows 10 Network Sharing
address 192.168.137.10
netmask 255.255.0.0
network 192.168.0.0
broadcast 192.168.255.255
gateway 192.168.137.1
nameserver 192.168.0.1
dns-nameserver 8.8.8.8 1.1.1.1
EOF
}

######################################################################
# Sync clock from NTP
######################################################################
function syncClockNTP() {
timedatectl set-ntp off 
ntpdate -s ntp.ubuntu.com 
timedatectl set-ntp on
timedatectl
}



######################################################################
# Raspberry PI GPS module control - NO LONGER USED
######################################################################
function initGPS() {
systemctl stop gpsd.socket
systemctl disable gpsd.socket
#killall gpsd

cat > /etc/default/gpsd << EOF
# Default settings for the gpsd init script and the hotplug wrapper.

# Start the gpsd daemon automatically at boot time
START_DAEMON="true"

# Use USB hotplugging to add new USB devices automatically to the daemon
USBAUTO="true"

# Devices gpsd should collect to at boot time.
# They need to be read/writeable, either by user gpsd or the group dialout.
DEVICES="/dev/ttyAMA0"

# Other options you want to pass to gpsd
#GPSD_OPTIONS="-D 2 -F /var/run/gpsd.sock"
GPSD_OPTIONS="-n -F /var/run/gpsd.sock"

EOF

systemctl enable gpsd.socket
systemctl start gpsd.socket 
}

		
######################################################################
# Main
# 
# $1 = Internet access = O/N
#
######################################################################
#
# Dev Mode : E = sortir a la première erreur, X = tracer chaque ligne
set -ex

vl_Internet=""$1

if [[ ${vl_Internet} == "O" ]]
then
	vl_useProxy=N

	echo "Is a proxy needed ? (O/N)"
	read vl_useProxy

	if [[ ${vl_useProxy} == "O" ]]
	then 
		setupProxy
	else
		unsetupProxy
	fi

	echo "Setting up Network..."
	setupNetwork

	echo "Resyncing Clock..."
	syncClockNTP

	# Misc installs
	apt-get install -y vim ntpdate minicom python-pip gpsd gpsd-clients python-gps telnet || echo "Error, return code  : " $?

fi


ulimit -c unlimited 

cd /home/pi/136-cloud-odyssey ; make ; cd -

# First boot
/home/pi/136-cloud-odyssey/iot-balise BOOT

echo "Waveshare HAT initialized"

# SMS to inform initial boot is complete
/home/pi/136-cloud-odyssey/iot-balise SMS "#IoT-Balise : Boot finished.
Local timestamp : $(date --iso-8601=seconds) 
--
Uptime : $(uptime)
-- Team 136 Cloud Odyssey --"

# Start daemon for GPS position
/home/pi/136-cloud-odyssey/iot-balise GPS

echo "End init script"

