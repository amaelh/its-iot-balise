/*  
 *  GPRS/GSM Quadband Module (SIM900)
 *  
 *  Copyright (C) Libelium Comunicaciones Distribuidas S.L. 
 *  http://www.libelium.com 
 *  
 *  This program is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or 
 *  (at your option) any later version. 
 *  a
 *  This program is distributed in the hope that it will be useful, 
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License 
 *  along with this program.  If not, see http://www.gnu.org/licenses/. 
 *  
 *  Version:           2.0
 *  Design:            David Gascón 
 *  Implementation:    Alejandro Gallego & Marcos Martinez
 *  
 ********************************************************************
 *  Customized version Sopra Steria Group - Team 136 Cloud Odyssey
 ********************************************************************
 */

#include <unistd.h>
//Include arduPi library
#include "arduPi.h"

// This needed for the mtrace() function
#include <mcheck.h>


/***************************************************************************/
// Functions declarations
/***************************************************************************/
int8_t sendATcommand2(const char* ATcommand, const char* expected_atAnswer1, const char* expected_atAnswer2, unsigned int timeout, char **pResult);
int8_t sendATcommandUntilMessage(const char* ATcommand, const char* expected_atAnswer1, unsigned int timeout);

/***************************************************************************/
// Constants
/***************************************************************************/
char configName[]="/home/pi/136-cloud-odyssey/iot-balise.cfg";
char logNamePrefix[]="/home/pi/136-cloud-odyssey/log/iot-balise.log.%04d";
char logName[256];

// Maximum retries
int gsmNetworkRetriesMax = 3; 
int gprsNetworkRetriesMax = 5;
int ipRetriesMax = 10;
int clockRetriesMax = 6;
int gpsRetriesMax = 10;
int gpsPositionRetriesMax = 1;

// Timeout for all AT commands
int atCommandTimeout = 2000; // miliseconds
// Power ON delay
int powerOnDelay = 10000; // miliseconds
// Delay between network connection interval
int networkDelay = 10000; // miliseconds
// Delay after sending SMS
int smsSendingTimeout = 10000; // miliseconds
// Delay for network clock
int gsmClockTimeout = 10000; // miliseconds
// HTTP responses are very long
int httpResponseTimeout = 30000; // miliseconds
// GPS position fix
int gpsTimeout = 10000; // miliseconds
// Main loop interval
int mainLoopInterval = 1000; // miliseconds

// voir raspberryPinNumber(...); : le pin 6 = gpio4
int onModulePin= 6;


/***************************************************************************/
// Global variables - yes this is messy and lazy but it does the work :)
/***************************************************************************/

// Configuration
char destinataire[32];
char pin[32];
char apn[32];
char user_name[32];
char password[32];
char posturl[1024]; 

// AT commands
char atCommandString[1024];
int8_t atAnswer;

// SMS 
char smsLine[4096];

// Position GPS
char gpsPositionString[512];
char gpsPosition[4096];
char* fix;
char* date;
char* latitude;
char* longitude;
char* altitude;

// Log File
FILE* fLog = NULL;
char logLine[4096];
char systemDate[256]; 

// Last known statuses
bool wasGSMUp = false;
bool wasGPRSUp = false;
bool wasGPSUp = false;


/**************************************
 * replace a character in a string 
 *************************************/
void str_replace(char *target, const char *needle, const char *replacement)
{
    char buffer[4096] = { 0 };
    char *insert_point = &buffer[0];
    const char *tmp = target;
    size_t needle_len = strlen(needle);
    size_t repl_len = strlen(replacement);

    while (1) 
    {
        const char *p = strstr(tmp, needle);

        // walked past last occurrence of needle; copy remaining part
        if (p == NULL) {
            strcpy(insert_point, tmp);
            break;
        }

        // copy part before needle
        memcpy(insert_point, tmp, p - tmp);
        insert_point += p - tmp;

        // copy replacement string
        memcpy(insert_point, replacement, repl_len);
        insert_point += repl_len;

        // adjust pointers, move on
        tmp = p + needle_len;
    }

    // write altered string back to target
    strcpy(target, buffer);
}

/***********************************
 * Gets and formats system date in the systemDate variable
 **********************************/
void formatSystemDate()
{
	time_t dateTime = time(NULL);
	struct tm tm = *localtime(&dateTime);

	sprintf(systemDate, "%d-%02d-%02d | %02d:%02d:%02d", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

void formatGpsPosition() 
{
	str_replace(gpsPositionString,"\"", " ");
	str_replace(gpsPositionString,"\'", " ");
	str_replace(gpsPositionString, "\r", "_");
	str_replace(gpsPositionString, "\n", "_");
	str_replace(gpsPositionString, ">", ")");
	str_replace(gpsPositionString, "OK", "YY");
}

/***********************************
 * Print to stdout + log file
 **********************************/
void log( const char* level, const char* message ) 
{
	char line[2048]; 

	formatSystemDate();
	sprintf(line, "%s | iot-balise | %s | %s\n", systemDate, level, message);

	// Open log file if needed
	if ( fLog == NULL )
	{
		// search for next log file name
		int i = 0;
		do {
			sprintf(logName, logNamePrefix, i);
			i++;
		} while ( access( logName, F_OK ) != -1 ) ;

		fLog = fopen(logName, "a");
	}

	//write to file if OK
	if ( fLog != NULL )
	{
		fputs( line, fLog );
		// flush at every line to be sure to have last state on disk in case of power failure.
		fflush( fLog );
	}
	else 
	{
		fprintf(stderr, "Impossible d'ecrire dans le fichier log\n");
	}

	// Also print to standard output because it's more convenient for debugging
	printf("%s",line);
}

void logDebug( const char* message ) 
{
	log("DEBUG", message);
}

void logInfo( const char* message ) 
{
	log("INFO", message);
}

void logNotice( const char* message ) 
{
	log("NOTICE", message);
}

void logWarning( const char* message ) 
{
	log("WARNING", message);
}

void logError( const char* message ) 
{
	log("ERROR", message);
}

/******************************
 * Checks if GSM is up
 *****************************/
bool isGSMUp()
{
	// Status 0,1 = registered, home network 
	// Status 0,5 = registered, Roaming 
	return ( ( sendATcommandUntilMessage("AT+CREG?", "+CREG: 0,5", atCommandTimeout ) || 
		   sendATcommandUntilMessage("AT+CREG?", "+CREG: 0,1", atCommandTimeout )) == 1 ); 
}

/******************************
 * Connects GSM network
 *****************************/
bool setupGSM()
{
	logInfo("setupGSM : start");

	// sets the PIN code
	sprintf(atCommandString, "AT+CPIN=%s", pin);
	// ignore response : if PIN incorrect then we don't want to loop, if PIN already unlocked then nothing more to do neither.
	sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout); 

	logInfo("Connecting to the network");
	// Wait for network
	int gsmNetworkRetries = 0;
	// Status 0,1 = registered, home network 
	// Status 0,5 = registered, Roaming 
	while ( ( sendATcommandUntilMessage("AT+CREG?", "+CREG: 0,1", atCommandTimeout ) || 
	 	  sendATcommandUntilMessage("AT+CREG?", "+CREG: 0,5", atCommandTimeout ) ) == 0 ) 
	{
		// check if maximum retries is reached
		if ( gsmNetworkRetries > gsmNetworkRetriesMax ) 
		{
			// return KO : cancel network connection for now
			logError("GSM network connection failed : maximum retries reached");
			return false;
		}	
		gsmNetworkRetries++; 

		logNotice("Waiting for network");
		delay(networkDelay);
	}

	logInfo("setupGSM : end");

	// Return OK : network connected
	return true;
}

/********************************
 * Send an SMS
 *******************************/
bool sms(char* phone_number, char* sms_text)
{
	//snprintf(logLine, sizeof(logLine), "sms : start. Original message to send before replacement of 'OK' : <%s>", sms_text );
	//log(logLine);

	logInfo("sms : start");

	// filter out all values that would be expected later in AT commands
	str_replace(sms_text, "OK", "YY");
	str_replace(sms_text, "\r", "_");
	str_replace(sms_text, "\n", "_");
	str_replace(sms_text, ">", ")");

	snprintf(logLine, sizeof(logLine), "Filtered out value to be sent : <%s>", sms_text );
	logDebug(logLine);

	sendATcommandUntilMessage("AT+CMGF=1", "OK", atCommandTimeout);    // sets the SMS mode to text

	sprintf(atCommandString,"AT+CMGS=\"%s\"", phone_number);
	atAnswer = sendATcommandUntilMessage(atCommandString, ">", atCommandTimeout);    // send the SMS number
	if (atAnswer == 1)
	{
		Serial.println(sms_text);
		Serial.write(0x1A);
		// sleep 2 seconds
		sleep(2);
		atAnswer = sendATcommandUntilMessage("", "OK", smsSendingTimeout );
		if (atAnswer == 1)
		{
			logInfo("SMS sent successfully");    
			return true;
		}
		else
		{
			logError("Error while sending SMS");
			return false;
		}
	}
	else
	{
		logError("Error while initialising SMS");
		return false;
	}

	// never reached
	return true;
}

/*****************************************
 * Returns true if GPRS is already up
 ****************************************/
bool isGPRSUp() 
{
	// if IP 0.0.0.0 is found then the function will return 1 and the test will be false
	return ( sendATcommandUntilMessage("AT+SAPBR=2,1", "0.0.0.0", atCommandTimeout ) == 0);
}

/*********************************
 * Setup GPRS
 ********************************/
bool setupGPRS()
{
	char* pResult = NULL;
	char *date; 
	char *vl_time; 
	char *line; 

	logInfo("setupGPRS : start");

	// sets APN , user name and password
	sendATcommandUntilMessage("AT+SAPBR=3,1,\"Contype\",\"GPRS\"", "OK", atCommandTimeout);
	snprintf(atCommandString, sizeof(atCommandString), "AT+SAPBR=3,1,\"APN\",\"%s\"", apn);
	sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout);

	snprintf(atCommandString, sizeof(atCommandString), "AT+SAPBR=3,1,\"USER\",\"%s\"", user_name);
	sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout);

	snprintf(atCommandString, sizeof(atCommandString), "AT+SAPBR=3,1,\"PWD\",\"%s\"", password);
	sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout);

	// waits for network
	int gprsNetworkRetries = 0; 
	while (sendATcommand2("AT+SAPBR=1,1", "OK", "ERROR", atCommandTimeout, &pResult ) == 0)
	{
		if ( gprsNetworkRetries > gprsNetworkRetriesMax ) 
		{
			// return KO : cancel network connection for now
			snprintf(smsLine, sizeof(smsLine), "#IoT-Balise : GPRS connection failed, maximum retries reached. Last response: %s -- Team 136 Cloud Odyssey --", pResult );
			// SMS already logs message
			sms(destinataire, smsLine);

			// free if KO and max retries reached
			free(pResult);

			return false;
		}	

		// Inform by SMS
		snprintf(smsLine, sizeof(smsLine), "#IoT-Balise : GPRS connection failed, trying again. Last response: %s -- Team 136 Cloud Odyssey --", pResult );
		sms(destinataire, smsLine);

		// free if KO
		free(pResult);

		gprsNetworkRetries++; 
		delay(networkDelay);
	}

	// free if OK
	free(pResult);

	int ipRetries = 0; 
	// Check if we have an IP adress : if yes then GPRS is connected
	while (sendATcommandUntilMessage("AT+SAPBR=2,1", "0.0.0.0", atCommandTimeout ) == 1)
	{
		if ( ipRetries > ipRetriesMax ) 
		{
			// return KO : cancel network connection for now
			snprintf(smsLine, sizeof(smsLine), "#IoT-Balise : IP assignation failed, maximum retries reached. -- Team 136 Cloud Odyssey --" );
			// SMS already logs message
			sms(destinataire, smsLine);

			return false;
		}	

		// Inform by SMS
		snprintf( smsLine, sizeof(smsLine), "#IoT-Balise : Still waiting for IP adress, trying again. -- Team 136 Cloud Odyssey --" );
		sms(destinataire, smsLine);

		ipRetries++; 
		delay(networkDelay);
	}

	// Inform by SMS
	snprintf( smsLine, sizeof(smsLine), "#IoT-Balise : GPRS connected and IP assigned. Now syncing system clock. -- Team 136 Cloud Odyssey --" );
	sms(destinataire, smsLine);

	/////////////////////////////////////////////////////////////////////////////////
	// Once GPRS is connected we ask for time to resync the raspberry system time
	/////////////////////////////////////////////////////////////////////////////////

	int clockRetries = 0; 
	int clockFound = 0;
	// GSM location and time
	while ( ( clockFound = sendATcommand2("AT+CIPGSMLOC=2,1", "+CIPGSMLOC:", "ERROR", gsmClockTimeout, &pResult ) ) != 1)
	{
		if ( clockRetries > clockRetriesMax ) 
		{
			// return KO : cancel network connection for now
			snprintf(smsLine, sizeof(smsLine), "#IoT-Balise : clock sync failed, maximum retries reached. Last response: <%s> -- Team 136 Cloud Odyssey --", pResult );
			// SMS already logs message
			sms(destinataire, smsLine);

			// free if KO
			free(pResult);

			break;
		}	

		// free if KO
		free(pResult);

		logNotice("Waiting for GPRS network time");

		delay(networkDelay);
		clockRetries++; 
	}

	// only parse if date found
	if ( clockFound == 1 ) 
	{
		// split at plus (+) sign
		line = strtok(pResult, "+");
		// search the bloc containing slashes (/) for the date
		while ( line != NULL && strchr(line, '/' ) == 0 ) {
			line = strtok(NULL, "+");
		}

		// if a result is found
		if ( line != NULL && strchr(line, '/') > 0 ) {
			// date is the 2nd token
			date = strtok(line, ",");
			date = strtok(NULL, ",");
			// time is the 3rd token
			vl_time = strtok(NULL, ",");

			// remove newline characters
			vl_time = strtok(vl_time, "\r");
			vl_time = strtok(vl_time, "\n");

			// Setting system time from GPRS network time
			snprintf(atCommandString, sizeof(atCommandString), "date --utc --set \"%s %s\" ", date, vl_time);
			logNotice(atCommandString);
			system(atCommandString);
		}

		// free memory from CIPGSMLOC command
		free(pResult);
	}

	time_t dateTime = time(NULL);
	struct tm tm = *localtime(&dateTime);

	// Inform by SMS
	snprintf( smsLine, sizeof(smsLine), "#IoT-Balise : End of GPRS connection sequence at local time : %d-%02d-%02d %02d:%02d:%02d -- Team 136 Cloud Odyssey --", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec );
	sms(destinataire, smsLine);

	logInfo("setupGPRS : end");

	// end OK
	return true;
}


/********************/
/* HTTP POST action */
/********************/
void postURL()
{
	char* pResult = NULL;

	logInfo("postURL : start");

	// first cancel all ongoing requests
	sendATcommandUntilMessage("AT+HTTPTERM", "OK", atCommandTimeout);

	// Initializes HTTP service
	atAnswer = sendATcommandUntilMessage("AT+HTTPINIT", "OK", atCommandTimeout );
	if (atAnswer == 1)
	{
		// Sets CID parameter
		atAnswer = sendATcommandUntilMessage("AT+HTTPPARA=\"CID\",1", "OK", atCommandTimeout );
		if (atAnswer == 1)
		{
			// Sets url 
			snprintf(atCommandString, sizeof(atCommandString), "AT+HTTPPARA=\"URL\",\"%s\"", posturl);
			atAnswer = sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout );
			if (atAnswer == 1)
			{
				// Sets content type (no error checking for this one) 
				snprintf(atCommandString, sizeof(atCommandString), "AT+HTTPPARA=\"CONTENT\",\"application/json\"" );
				atAnswer = sendATcommandUntilMessage(atCommandString, "OK", atCommandTimeout );

				// Sets the POST Data 
				snprintf(atCommandString, sizeof(atCommandString), "AT+HTTPDATA=%zd,10000", strlen(gpsPosition)+10 );
				atAnswer = sendATcommandUntilMessage(atCommandString, "DOWNLOAD", atCommandTimeout );
				if (atAnswer == 1)
				{
					logDebug( gpsPosition );
					Serial.println(gpsPosition);    // Send the AT command 
					atAnswer = sendATcommandUntilMessage("", "OK", httpResponseTimeout);

					// Starts POST action
					atAnswer = sendATcommandUntilMessage("AT+HTTPACTION=1", "+HTTPACTION: 1,200", httpResponseTimeout );
					if (atAnswer == 1)
					{
						// Reads HTTP Content
						sprintf(atCommandString, "AT+HTTPREAD" );
						if (sendATcommand2(atCommandString, "+HTTPREAD:", "ERROR", httpResponseTimeout, &pResult ) == 1)
						{
							logInfo("HTTP Read OK");
						}
						else if (atAnswer == 2)
						{
							logError("Error from HTTP");
						}
						// discard result
						free(pResult);
					}
					else
					{
						logError("Error posting to url");
					}
				}
			}
			else
			{
				logError("Error setting the url");
			}
		}
		else
		{
			logError("Error setting the CID");
		}    
	}
	else
	{
		logError("Error initializating");
	}

	sendATcommandUntilMessage("AT+HTTPTERM", "OK", atCommandTimeout);

	logInfo("postURL : end");
}


/*********************************************
 * GPS part
 ********************************************/
bool powerGPS()
{
	logInfo("powerGPS : start");

	// Check GPS power
	int gpsRetries = 0;
	while (sendATcommandUntilMessage("AT+CGNSPWR=1", "OK", atCommandTimeout ) == 0)
	{
		logNotice("Waiting for GPS power status");
		delay(gpsTimeout);

		if ( gpsRetries > gpsRetriesMax ) 
		{
			// return KO
			logError("GPS Power ON failed : maximum retries reached");
			return false;
		}	
		gpsRetries++; 
	}

	logInfo("powerGPS : end");

	return true;
}


/************************************************************
 * Gets GPS signal and stores values in global variables
 ***********************************************************/
bool getGPS()
{
	char* pResult = NULL;
	char* line;

	logInfo("getGPS : start");

	powerGPS();

	// On demande la position GPS
	int gpsPositionRetries = 0;
	while ( sendATcommand2( "AT+CGPSSTATUS?", "2D Fix", "3D Fix", atCommandTimeout, &pResult ) == 0) 
	{
		free(pResult);

		// get and log infos for debugging purpose
		//sendATcommand2("AT+CGNSINF", "OK", "OK", atCommandTimeout, &pResult ); 
		sendATcommand2("AT+CGNSINF", ",2018", "ERROR", atCommandTimeout, &pResult );
		sprintf(gpsPositionString, "%s", pResult);

		// No maximum tries if max == -1 : this is default value for production as gps positionning is the whole purpose of the program !
		if ( gpsPositionRetriesMax != -1 && gpsPositionRetries > gpsPositionRetriesMax ) 
		{
			// return KO
			logWarning("GPS position unavailable : no fix and maximum retries reached");

			formatSystemDate();
			formatGpsPosition();
			snprintf(gpsPosition, sizeof(gpsPosition), "{ \"node\": \"2\", \"fix_status\": \"0\", \"systemDate\": \"%s\", \"gpsDate\": \"\", \"latitude\": \"\", \"longitude\": \"\", \"altitude\": \"\", \"fullMessage\": \"%s\", \"url\": \"\" }", systemDate, gpsPositionString );
			logDebug(gpsPosition);

			// free if KO
			free(pResult);

			return false;
		}	

		logNotice("Waiting for GPS 2D or 3D fix");
		gpsPositionRetries++; 
		delay(gpsTimeout);

		// free if KO
		free(pResult);
	}

	// free if OK
	free(pResult);

	// store timestamp for next reboot
	sendATcommandUntilMessage("AT+CLTS=1", "OK", atCommandTimeout);
	sendATcommandUntilMessage("AT&W", "OK", atCommandTimeout);

	// On demande la position GPS
	while (sendATcommand2("AT+CGNSINF", ",2018", "ERROR", atCommandTimeout, &pResult ) != 1)
	{
		// save line for later use
		sprintf(gpsPositionString, "%s", pResult);

		// No maximum tries if max == -1 : this is default value for production as gps positionning is the whole purpose of the program !
		if ( gpsPositionRetriesMax != -1 && gpsPositionRetries > gpsPositionRetriesMax ) 
		{
			// return KO
			logWarning("GPS position unavailable : incorrect timestamp in satelitte response and maximum retries reached");

			formatSystemDate();
			formatGpsPosition();
			snprintf(gpsPosition, sizeof(gpsPosition), "{ \"node\": \"2\", \"fix_status\": \"0\", \"systemDate\": \"%s\", \"gpsDate\": \"\", \"latitude\": \"\", \"longitude\": \"\", \"altitude\": \"\", \"fullMessage\": \"%s\", \"url\": \"\" }", systemDate, gpsPositionString );
			logDebug(gpsPosition);

			// free if KO and max reached
			free(pResult);

			return false;
		}	

		logNotice("Waiting for GPS position containing digits '2018'");
		gpsPositionRetries++; 
		delay(gpsTimeout);

		// free if KO
		free(pResult);
	}

	// save line for later use
	sprintf(gpsPositionString, "%s", pResult);

	// we split at the plus (+) sign
	line = strtok(pResult, "+");
	// we search for blocs containing dots (.) : GPS position
	while ( line != NULL && strchr(line, '.' ) == 0 ) {
		line = strtok(NULL, "+");
	}

	// if a result is found
	if ( line != NULL && strchr(line, '.') > 0 ) {
		// +CGNSINF: <GNSS run status>,<Fix status>, <UTC date & Time>,<Latitude>,<Longitude>, <MSL Altitude>,<Speed Over Ground>, <Course Over Ground>, <Fix Mode>,<Reserved1>,<HDOP>,<PDOP>, <VDOP>,<Reserved2>,<GNSS Satellites in View>, <GNSS Satellites Used>,<GLONASS Satellites Used>,<Reserved3>,<C/N0 max>,<HPA>,<VPA>
		// fix status : 2d token
		fix = strtok(line, ",");
		fix = strtok(NULL, ",");
		// date : 3d token
		date = strtok(NULL, ",");
		// latitude : 4th token
		latitude = strtok(NULL, ",");
		// longitude : 5th token
		longitude = strtok(NULL, ",");
		// altitude : 6th token
		altitude = strtok(NULL, ",");

		/* store GPS position in JSON format in global variable */
		formatSystemDate();
		formatGpsPosition();
		snprintf(gpsPosition, sizeof(gpsPosition), "{ \"node\": \"2\", \"fix_status\": \"%s\", \"systemDate\": \"%s\", \"gpsDate\": \"%s\", \"latitude\": \"%s\", \"longitude\": \"%s\", \"altitude\": \"%s\", \"fullMessage\": \"%s\", \"url\": \"http://maps.google.com/maps?q=%s,%s\" }", fix, systemDate, date , latitude, longitude, altitude, gpsPositionString, latitude, longitude );

		logDebug(gpsPosition);

	}
	
	// free CGNSINF result
	free(pResult);

	logInfo("getGPS : end");

	return true;
}


/******************************
 * Power ON for the HAT
 ******************************/
void powerModuleIfNeeded()
{
	uint8_t atAnswer=0;

	logInfo("powerModuleIfNeeded : start");

	// checks if the module is started
	atAnswer = sendATcommandUntilMessage("AT", "OK", atCommandTimeout);

	// waits for an atAnswer from the module
	while(atAnswer == 0)
	{  
		logNotice("Power check : Waiting for AT response OK");

		// power on pulse
		digitalWrite(onModulePin,LOW);
		delay(powerOnDelay);
		digitalWrite(onModulePin,HIGH);

		// Send AT and wait for the atAnswer   
		atAnswer = sendATcommandUntilMessage("AT", "OK", atCommandTimeout);    

		// No maximum tries here : this is the only purpose of the program !
	}

	// power on GPS at module power up to speed up acquisition time
	powerGPS();

	logInfo("powerModuleIfNeeded : end");
}


/**********************************************************************
 * AT command function : sends a message and returns when the 
 * expected response is read from serial or timeout is reached
 *********************************************************************/
int8_t sendATcommandUntilMessage(const char* ATcommand, const char* expected_atAnswer1, unsigned int timeout)
{
	uint8_t x=0,  atAnswer=0;
	char response[2000];
	unsigned long previous;

	logInfo("sendATcommandUntilMessage : start");

	memset(response, '\0', 2000);    // Initialize the string

	// Clear buffer before
	while(Serial.available() != 0){    
		Serial.read();
	}

	Serial.println(ATcommand);    // Send the AT command 
	sleep( 1 );

	x = 0;
	previous = millis();

	// this loop waits for the atAnswer
	do{
		if(Serial.available() != 0){    
			response[x] = Serial.read();
			//printf("%c",response[x]);
			x++;
			// check if the desired atAnswer is in the response of the module
			if (strstr(response, expected_atAnswer1) != NULL)    
			{
				atAnswer = 1;
				//printf("Response <%s> found in <%s>\n", expected_atAnswer1, response);
			}
		}
		// Waits for the asnwer with time out
	} while ( atAnswer == 0 && (millis() - previous) < timeout );    

	response[x++] = '\0';

	delay(1000);

	// Clear buffer for next calls
	while(Serial.available() != 0){    
		Serial.read();
	}

	str_replace(response, "\r", "_");
	str_replace(response, "\n", "_");
	snprintf(logLine, sizeof(logLine), "%s", response );
	logDebug(logLine);

	snprintf(logLine, sizeof(logLine), "sendATcommandUntilMessage : end." );
	logInfo(logLine);

	return atAnswer;
}


/*************************************************************************************************************
 * AT command function : sends a message, waits a specified amount of time then returns
 * 
 * Return value indicates which of the two expected answers are found in response, 0 if none
 * 
 * Full command + response is also returned in **pRet => Don't forget to free this pointer afterwards !
 *************************************************************************************************************/
int8_t sendATcommand2(const char* ATcommand, const char* expected_atAnswer1, const char* expected_atAnswer2, unsigned int timeout, char **pRet)
{
	uint8_t x=0,  atAnswer=0;
	char* response = new char[1000];

	logInfo("sendATcommand2 : start");

	memset(response, '\0', 1000);    // Initialize the string

	//Clear buffer before
	while(Serial.available() != 0)
	{
		Serial.read();
	}

	Serial.println(ATcommand);    // Send the AT command 

	x = 0;

	//Sleep an arbitrary amount of time, then parse the response
	sleep( timeout / 1000 );

	while( Serial.available() != 0)
	{
		response[x] = Serial.read();
		x++;
	}

	// check if the desired atAnswer 1 is in the response of the module
	if (strstr(response, expected_atAnswer1) != NULL)    
	{
		atAnswer = 1;
	}  
	// check if the desired atAnswer 2 is in the response of the module
	else if (strstr(response, expected_atAnswer2) != NULL)    
	{
		atAnswer = 2;
	}

	*pRet = response;

	delay(1000);

	// Clear buffer for next calls
	while(Serial.available() != 0){    
		Serial.read();
	}

	str_replace(response, "\r", "_");
	str_replace(response, "\n", "_");
	snprintf(logLine, sizeof(logLine), "%s", response );
	logDebug(logLine);

	snprintf(logLine, sizeof(logLine), "sendATcommand2 : end.");
	logInfo(logLine);

	return atAnswer;
}


/***********************************
 * Lecture fichier de configuration 
 ***********************************/
void readConfig() 
{
	FILE* fConf;
	char name[128];
	char value[128];
	char line[1024];

	snprintf(logLine, sizeof(logLine), "readConfig : start. Configuration file : <%s>", configName );
	logInfo(logLine);

	fConf = fopen(configName, "r");

	if ( fConf != NULL )
	{
		//while ( fscanf(fConf, "\n%127[^=]=%127[^\n]\n", name, value) != EOF )
		while ( fgets(line, sizeof(line), fConf) != NULL )
		{
			// ignore comments, blank lines
			if (line[0] != '#' && line[0] != ' ' && line[0] != '\n') 
			{
				
				memset(name, '\0', 128);    // Initialize the string
				memset(value, '\0', 128);    // Initialize the string

				sscanf(line, "%127[^=]=%127[^\n]", name, value);

				if ( strcmp(name,"pin") == 0 ) {
					strcpy(pin, value);

					snprintf(logLine, sizeof(logLine), "Configuration : pin=<%s>", pin );
					logInfo(logLine);
				}
				if ( strcmp(name,"apn") == 0 ) {
					strcpy(apn, value);

					snprintf(logLine, sizeof(logLine), "Configuration : apn=<%s>", apn );
					logInfo(logLine);
				}
				if ( strcmp(name,"user_name") == 0 ) {
					strcpy(user_name, value);

					snprintf(logLine, sizeof(logLine), "Configuration : user_name=<%s>", user_name );
					logInfo(logLine);
				}
				if ( strcmp(name,"password") == 0 ) {
					strcpy(password, value);

					snprintf(logLine, sizeof(logLine), "Configuration : password=<%s>", password );
					logInfo(logLine);
				}
				if ( strcmp(name,"destinataire") == 0 ) {
					strcpy(destinataire, value);

					snprintf(logLine, sizeof(logLine), "Configuration : destinataire=<%s>", destinataire );
					logInfo(logLine);
				}
				if ( strcmp(name,"posturl") == 0 ) {
					strcpy(posturl, value);

					snprintf(logLine, sizeof(logLine), "Configuration : posturl=<%s>", posturl );
					logInfo(logLine);
				}
			}
		}
		fclose(fConf);
	}
	else
	{
		fprintf(stderr, "Impossible d'ouvrir le fichier de configuration\n");
		logError("Impossible d'ouvrir le fichier de configuration");
	}

	logInfo("readConfig : end");
}


/******************************************************************
 * Reconnect all networks 
 *
 * @return : 0 = No Network, 1 = GSM only, 2 = GPRS
******************************************************************/
int networkReconnectSequence()
{
	char networkSequenceMessage[1024];
	int ret = 0; // default : no network

	logInfo("networkReconnectSequence : start");

	bool gsmOK = isGSMUp();
	bool gprsOK = isGPRSUp();

	// checks if GSM is already up and tries to connect if no
	if ( !gsmOK ) 
	{
		// If down, try to reconnect and send SMS
		gsmOK = setupGSM();

		// without GSM it's useless to connect GPRS or to send SMS messages
		if (gsmOK) 
		{
			ret = 1; // 1 = GSM OK
			strcpy(networkSequenceMessage,"#IoT-Balise : Network step 1 -> GSM UP again -- Team 136 Cloud Odyssey --");
			sms(destinataire, networkSequenceMessage);
		}
		else
		{
			ret = 0; // 0 = no network
			logWarning("networkReconnectSequence : Network step 1 FAILED -- GSM still DOWN");
			// no SMS : useless
		}
	}

	// without GSM it's useless to connect GPRS or to send SMS messages
	if (gsmOK) 
	{
		ret = 1; // 1 = GSM OK

		// If GPRS already UP, don't do anything
		if ( !gprsOK ) 
		{
			bool gprsOK = setupGPRS();
			if (gprsOK)
			{
				strcpy(networkSequenceMessage,"#IoT-Balise : Network step 2 -> GPRS UP again -- Team 136 Cloud Odyssey --");
				ret = 2; // 2 = GPRS connected
			}
			else
			{
				strcpy(networkSequenceMessage,"#IoT-Balise : Network step 2 FAILED -> GPRS still DOWN -- ERROR - Unable to connect after max retries -- Team 136 Cloud Odyssey --");
			}
			// send state by SMS
			sms(destinataire, networkSequenceMessage);
		}
		else 
		{
			ret = 2; // 1 = GSM OK
		}
	}

	// log current status
	snprintf(logLine, sizeof(logLine), "networkReconnectSequence : end. Network status : GSM:<%d>, GPRS:<%d>", gsmOK, gprsOK );
	logInfo(logLine);

	return ret;
}


/***********************************
 * Main
 ***********************************/
int main (int argc, char* argv[])
{
	mtrace();

	char* mode; 
	char* text; 

	////////////////////////////////////////////////
	// Lecture des parametres d'appel 
	////////////////////////////////////////////////
	if (argc < 2) {
		fprintf (stderr, "Utilisation: %s mode={BOOT,SMS,GPS} [text]\n", argv[0]);
		return EXIT_FAILURE;
	}

	// lecture Mode
	mode = argv[1];

	snprintf(logLine, sizeof(logLine), "Starting up in mode : <%s>", mode );
	logNotice(logLine);

	// lecture texte
	if (argc > 1) {
		// lecture texte
		text = argv[2];
		snprintf(logLine, sizeof(logLine), "Text provided in parameters : <%s>", text );
		logInfo(logLine);
	}

	////////////////////////////////////////////////
	// Mode ecriture vers le HAT
	////////////////////////////////////////////////
	pinMode(onModulePin, OUTPUT);
	Serial.begin(115200);    

	// Allumage du module et du GPS
	powerModuleIfNeeded();

	// Lecture configuration
	readConfig();

	////////////////////////////////////////////////
	// Appel des fonctions
	////////////////////////////////////////////////
	if ( strcmp(mode, "BOOT") == 0 ) 
	{
		networkReconnectSequence();
	}

	// Simply send an SMS with GSM - no need for full network sequence
	if ( strcmp(mode, "SMS") == 0 ) 
	{
		// checks if GSM is already up and tries to connect if no
		if ( !isGSMUp() ) 
		{
			// If down, try to reconnect and send SMS
			bool gsmOK = setupGSM();

			// without GSM it's useless to connect GPRS or to send SMS messages
			if (gsmOK) 
			{
				logNotice("Main SMS -> GSM UP again -- Team 136 Cloud Odyssey");
			}
			else
			{
				logWarning("Main SMS -> GSM DOWN -- FAILED");
			}
		}

		sms(destinataire, text);
	}

	// GPS daemon
	if ( strcmp(mode, "GPS") == 0 ) 
	{
		// Loop on the next commands, store result of getGPS in file
		do 
		{
			logDebug("Main loop : begin");

			int result = networkReconnectSequence();

			snprintf(logLine, sizeof(logLine), "Main loop : network sequence end. Result : <%d>", result );
			logInfo(logLine);

			// always get GPS values : it's the reason to be of this module
			getGPS();

			// if GSM connected
			if ( result >= 1 ) 
			{
				logDebug("Main loop : sending by SMS");

				// send by SMS
				sms(destinataire, gpsPosition);

				// if GPRS connected
				if ( result == 2 ) 
				{
					logDebug("Main loop : posting to webserver");
					// post to webserver
					postURL();
				}
				else
				{
					logWarning("Main loop : GPRS is down, position has NOT been posted to server");
				}
			}
			else
			{
				logWarning("Main loop : GSM is down, position has NOT been sent by ANY means");
			}

			snprintf(logLine, sizeof(logLine), "Main loop : sleep for <%d> milliseconds", mainLoopInterval );
			logInfo(logLine);

			// Sleep interval
			delay(mainLoopInterval);

		} while (true);	
	}

	return (0);
}


